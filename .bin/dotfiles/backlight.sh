#!/bin/bash

function send_notification {
    dunstify -t 3000 -r 2414 -u normal "LCD Backlight: $(printf %.0f%% $(xbacklight -get))"

}

case $1 in
    up)
        xbacklight -inc 5 > /dev/null
		send_notification
		;;
    down)
        xbacklight -dec 5 > /dev/null
        send_notification
		;;
esac

