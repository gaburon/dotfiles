#!/usr/bin/env bash

SCRIPT_PATH="$HOME/.bin/dotfiles/polybar"
FILE="$SCRIPT_PATH/rofi/colors.rasi"

rofi -no-config -no-lazy-grab -show drun -modi drun -theme $SCRIPT_PATH/rofi/launcher.rasi
