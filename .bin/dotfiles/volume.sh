#!/bin/bash

function get_volume {
    amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
}

function is_mute {
    amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
}

function send_notification {
    volume=`get_volume`
    dunstify -i audio-volume-muted-blocking -t 3000 -r 2494 -u normal "Volume: $volume%"
}

case $1 in
    up)
		amixer -D pulse set Master on > /dev/null
		amixer -D pulse sset Master 5%+ > /dev/null
		send_notification
		;;
    down)
		amixer -D pulse set Master on > /dev/null
		amixer -D pulse sset Master 5%- > /dev/null
		send_notification
		;;
    mute)
		amixer -D pulse set Master 1+ toggle > /dev/null
		if is_mute ; then
	    	dunstify -i audio-volume-muted -t 3000 -r 2494 -u normal "Mute"
		else
	    	send_notification
		fi
		;;
esac
