#!/usr/bin/env bash

pkill -f py3Term
while pgrep -u $UID -f py3Term >/dev/null; do sleep 1; done
termite --exec=python3 --name=py3Term
