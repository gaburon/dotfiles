#!/usr/bin/env bash

pkill -f calTerm
while pgrep -u $UID -f calTerm >/dev/null; do sleep 1; done
termite --exec="cal -3" --name=calTerm --hold 
